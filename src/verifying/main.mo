import SHA224 "SHA224";
import Hex "Hex";
import Blob "mo:base/Blob";

actor Verify {
    type SeedPair = {
        seed1 : Blob;
        seed2 : Blob;
    };

    type HashPair = {
        hash1 : Text;
        hash2 : Text;
    };

    public shared func verify(seeds : SeedPair) : async HashPair {
        let firstHash = Hex.encode(SHA224.sha224(Blob.toArray(seeds.seed1)));
        let secondHash = Hex.encode(SHA224.sha224(Blob.toArray(seeds.seed2)));

        let thisHashes : HashPair = {
            hash1 = firstHash;
            hash2 = secondHash;
        };

        return thisHashes;

    };
}